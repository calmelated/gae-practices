package com.appspot.chadgae.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.appspot.chadgae.shared.Member;
import com.appspot.chadgae.shared.PMF;

@PersistenceCapable
public class Member implements Serializable {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	Long id;

	@Persistent
	String userid;

	@Persistent
	String password;

	@Persistent
	String nickname;

	@Persistent
	String email;

	@NotPersistent
	List<Member> members;

	public Member() {
	}
	
	public Member(String userid, String password, String email) {
		super();
		this.userid = userid;
		this.password = password;
		this.email = email;
	}	

	public Member(String userid, String password, String nickname, String email) {
		super();
		this.userid = userid;
		this.password = password;
		this.nickname = nickname;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Member> getMembers() {
		if (members == null) {
			PersistenceManager pm = PMF.get().getPersistenceManager();
			Query query = pm.newQuery("SELECT FROM " + Member.class.getName());
			members = (List<Member>) query.execute();
		}
		return members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}
	
	public boolean save(){
		boolean saved = false;
		
		//查詢userId為登入者的E-mail實例
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery(Member.class);
		query.setFilter("userid == uid");
		query.declareParameters("String uid");
		
		//傳入目前的userId,進行查詢並取得Member集合
		List<Member> members = (List<Member>) query.execute(this.userid);
		if (members.isEmpty()){
			pm.makePersistent(this);
			saved = true;
		}
		return saved;
	}	

}
