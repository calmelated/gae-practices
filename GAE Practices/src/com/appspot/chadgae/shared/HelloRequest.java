package com.appspot.chadgae.shared;

import java.util.List;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.ServiceName;

@ServiceName(value = "com.gb.server.HelloService", locator = "com.gb.server.HelloServiceLocator")
public interface HelloRequest extends RequestContext {

	Request<ContactProxy> createContact();

	Request<ContactProxy> readContact(Long id);

	Request<ContactProxy> updateContact(ContactProxy contact);

	Request<Void> deleteContact(ContactProxy contact);

	Request<List<ContactProxy>> queryContacts();

}
