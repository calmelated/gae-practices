package com.appspot.chadgae.shared;

import java.util.List;
import javax.jdo.PersistenceManager;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;

public class ContactCRUD {

	public static void main(String[] args) {
	}

	public void testCreate() {
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Contact tom = new Contact("Tom", "tom@jo87.com", "12312321");
		pm.makePersistent(tom);
//		Query query = pm.newQuery("select from Contact");
//		List<Contact> result = (List<Contact>) query.execute();
//		System.out.println(result.get(0).name);
//		assertEquals(1, ds.prepare(new Query("Contact")).c);
	}

//	@Test
//	public void testRead() {
//		PersistenceManager pm = PMF.get().getPersistenceManager();
//		Query query = pm.newQuery("select from Contact");
//		List<Contact> result = (List<Contact>) query.execute();
//		System.out.println(result.get(0).name);
//	}

	public void createAndGet(){
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Contact contact = new Contact("Tom", "tom@jo87.com", "13123123");
		pm.makePersistent(contact);
		pm.close();

		pm = PMF.get().getPersistenceManager();
		Contact data = pm.getObjectById(Contact.class, contact.id);
		System.out.println(data.name);
		pm.close();
	}
}
