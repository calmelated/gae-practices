package com.appspot.chadgae.shared;

import com.google.web.bindery.requestfactory.shared.RequestFactory;


public interface HelloRequestFactory extends RequestFactory {

	HelloRequest helloRequest();

}
