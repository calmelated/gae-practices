package com.appspot.chadgae.shared;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "com.gb.Contact", locator = "com.gb.ContactLocator")
public interface ContactProxy extends ValueProxy {

	Long getId();

	String getName();

	void setName(String name);

	String getEmail();

	void setEmail(String email);

	String getAddress();

	void setAddress(String address);

}
