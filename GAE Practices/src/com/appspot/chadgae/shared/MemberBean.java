package com.appspot.chadgae.shared;

import java.util.List;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
public class MemberBean {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	Long id;

	@Persistent
	String userid;

	@Persistent
	String password;

	@Persistent
	String nickname;

	@Persistent
	String email;
	
	@NotPersistent
	List<Member> allMembers;
	
	public List<Member> getAllMembers() {
		return allMembers;
	}
	
	public void setAllMembers(List<Member> allMembers) {
		this.allMembers = allMembers;
	}
	
	public MemberBean(String userid, String password, String nickname, String email) {
		super();
		this.userid = userid;
		this.password = password;
		this.nickname = nickname;
		this.email = email;
	}
	
	public MemberBean(){
		
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUserid() {
		return userid;
	}
	
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getNickname() {
		return nickname;
	}
	
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
}
