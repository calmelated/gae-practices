package com.appspot.chadgae.shared;

import com.google.web.bindery.requestfactory.shared.Locator;


public class ContactLocator extends Locator<Contact, Void> {

	@Override
	public Contact create(Class<? extends Contact> clazz) {
		// TODO no default constructor, creation code cannot be generated
		throw new RuntimeException(String.format("Cannot instantiate %s",
				clazz.getCanonicalName()));
	}

	@Override
	public Contact find(Class<? extends Contact> clazz, Void id) {
		return create(clazz);
	}

	@Override
	public Class<Contact> getDomainType() {
		return Contact.class;
	}

	@Override
	public Void getId(Contact domainObject) {
		return null;
	}

	@Override
	public Class<Void> getIdType() {
		return Void.class;
	}

	@Override
	public Object getVersion(Contact domainObject) {
		return null;
	}

}
