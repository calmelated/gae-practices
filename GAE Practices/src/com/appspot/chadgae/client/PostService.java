package com.appspot.chadgae.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.appspot.chadgae.shared.Post;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("post")
public interface PostService extends RemoteService {
	public boolean savePost(Post post);

	public boolean updatePost(Post post);

	List<Post> getAllPosts();

	public boolean deletePost(Post post);
}
