package com.appspot.chadgae.client;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;

public class AddPost extends Composite {
	public AddPost() {

		FlexTable flexTable = new FlexTable();
		initWidget(flexTable);
		flexTable.setSize("435px", "293px");

		Label label = new Label("匿稱:");
		flexTable.setWidget(0, 0, label);

		TextBox textBox = new TextBox();
		flexTable.setWidget(0, 1, textBox);

		Label label_1 = new Label("標題:");
		flexTable.setWidget(1, 0, label_1);

		TextBox textBox_1 = new TextBox();
		flexTable.setWidget(1, 1, textBox_1);

		Label label_2 = new Label("留言內容:");
		flexTable.setWidget(2, 0, label_2);

		TextArea textArea = new TextArea();
		flexTable.setWidget(2, 1, textArea);
		textArea.setSize("298px", "120px");

		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		flexTable.setWidget(3, 0, horizontalPanel);
		horizontalPanel.setWidth("400px");

		Button button = new Button("新增");
		horizontalPanel.add(button);

		Button button_1 = new Button("取消");
		button_1.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
			}
		});
		horizontalPanel.add(button_1);
		flexTable.getFlexCellFormatter().setColSpan(3, 0, 2);

	}
}
