package com.appspot.chadgae.client;

import com.appspot.chadgae.shared.Post;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;

public class PostDetail extends Composite {
	private Label date;
	private Label nickname;
	private Label title;
	private Label context;

	/**
	 * @wbp.parser.constructor
	 */
	public PostDetail() {
		initWidgets();
	}

	public PostDetail(Post post) {
		initWidgets();
		date.setText(post.getDate().toString());
		nickname.setText(post.getNickname());
		title.setText(post.getTitle());
		context.setText(post.getContent());
	}

	public void initWidgets() {
		FlexTable flexTable = new FlexTable();
		initWidget(flexTable);

		Label lblNewLabel = new Label("留言時間:");
		flexTable.setWidget(0, 0, lblNewLabel);

		date = new Label("New label");
		flexTable.setWidget(0, 1, date);

		Label lblNewLabel_1 = new Label("匿稱:");
		flexTable.setWidget(1, 0, lblNewLabel_1);

		nickname = new Label("New label");
		flexTable.setWidget(1, 1, nickname);

		Label label_1 = new Label("標題:");
		flexTable.setWidget(2, 0, label_1);

		title = new Label("New label");
		flexTable.setWidget(2, 1, title);

		Label label_2 = new Label("留言內容:");
		flexTable.setWidget(3, 0, label_2);

		context = new Label("New label");
		context.setWordWrap(false);
		flexTable.setWidget(3, 1, context);
		context.setSize("221px", "53px");
	}
}
