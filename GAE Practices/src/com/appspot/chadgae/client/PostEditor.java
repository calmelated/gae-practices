package com.appspot.chadgae.client;

import com.appspot.chadgae.shared.Post;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextArea;

public class PostEditor extends Composite {
	private DockPanel dockPanel;
	private TextBox nickname;
	private TextBox title;
	private TextArea content;
	Post post;

	public PostEditor() {
		dockPanel = new DockPanel();
		initWidget(dockPanel);

		FlexTable flexTable = new FlexTable();
		dockPanel.add(flexTable, DockPanel.CENTER);

		Label label = new Label("匿稱:");
		flexTable.setWidget(0, 0, label);
		nickname = new TextBox();
		flexTable.setWidget(0, 1, nickname);

		Label label_1 = new Label("標題:");
		flexTable.setWidget(1, 0, label_1);
		title = new TextBox();
		flexTable.setWidget(1, 1, title);

		Label lblNewLabel = new Label("留言內容:");
		flexTable.setWidget(2, 0, lblNewLabel);
		content = new TextArea();
		flexTable.setWidget(2, 1, content);
		content.setSize("180px", "77px");
	}

	public void setPost(Post post) {
		this.post = post;
		nickname.setText(post.getNickname());
		title.setText(post.getTitle());
		content.setText(post.getContent());
	}

	public Post getPost() {
		post.setNickname(nickname.getText());
		post.setTitle(title.getText());
		post.setContent(content.getText());
		return post;
	}

	public DockPanel getDockPanel() {
		return dockPanel;
	}

	public void setDockPanel(DockPanel dockPanel) {
		this.dockPanel = dockPanel;
	}
}
