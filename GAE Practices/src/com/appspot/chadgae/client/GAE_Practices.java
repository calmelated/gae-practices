package com.appspot.chadgae.client;

import java.util.List;

import com.appspot.chadgae.shared.Contact;
import com.appspot.chadgae.shared.FieldVerifier;
import com.appspot.chadgae.shared.Post;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.HTMLPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GAE_Practices implements EntryPoint, ClickHandler {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while " + "attempting to contact the server. Please check your network " + "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	
	private int selectRow = -1;
	private DockPanel dockPanel;
	private List<Post> posts;
	private Widget centerWidget;
	private FlexTable flexTable;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		// Add the nameField and sendButton to the RootPanel
		// Use RootPanel.get() to get the entire body element
		RootPanel rootPanel = RootPanel.get();
		final TextBox nameField = new TextBox();
		final Button sendButton = new Button("Send");
		final Label errorLabel = new Label();
		nameField.setText("GWT User");
		Contact contact = new Contact("chad", "chad@ksmot.com.tw", "123123123", "http://j.snpy.org/worker.png");
		
		LoginPanel loginPanel = new LoginPanel();
		rootPanel.add(loginPanel, 0, 0);
		loginPanel.setSize("741px", "58px");
		
		// Create the popup dialog box
		final DialogBox dialogBox = new DialogBox();
		dialogBox.setText("Remote Procedure Call");
		dialogBox.setAnimationEnabled(true);
		
		final Button closeButton = new Button("Close");
		// We can set the id of a widget by accessing its Element
		closeButton.getElement().setId("closeButton");
		
		final Label textToServerLabel = new Label();
		final HTML serverResponseLabel = new HTML();
		VerticalPanel dialogVPanel = new VerticalPanel();
		dialogVPanel.addStyleName("dialogVPanel");
		dialogVPanel.add(new HTML("<b>Sending name to the server:</b>"));
		dialogVPanel.add(textToServerLabel);
		dialogVPanel.add(new HTML("<br><b>Server replies:</b>"));
		dialogVPanel.add(serverResponseLabel);
		dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
		dialogVPanel.add(closeButton);
		dialogBox.setWidget(dialogVPanel);

		// Add a handler to close the DialogBox
		closeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
				sendButton.setEnabled(true);
				sendButton.setFocus(true);
			}
		});

		// Create a handler for the sendButton and nameField
		class MyHandler implements ClickHandler, KeyUpHandler {
			/**
			 * Fired when the user clicks on the sendButton.
			 */
			public void onClick(ClickEvent event) {
				sendNameToServer();
			}

			/**
			 * Fired when the user types in the nameField.
			 */
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					sendNameToServer();
				}
			}

			/**
			 * Send the name from the nameField to the server and wait for a response.
			 */
			private void sendNameToServer() {
				// First, we validate the input.
				errorLabel.setText("");
				String textToServer = nameField.getText();
				if (!FieldVerifier.isValidName(textToServer)) {
					errorLabel.setText("Please enter at least four characters");
					return;
				}

				// Then, we send the input to the server.
				sendButton.setEnabled(false);
				textToServerLabel.setText(textToServer);
				serverResponseLabel.setText("");
				greetingService.greetServer(textToServer, new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user
						dialogBox.setText("Remote Procedure Call - Failure");
						serverResponseLabel.addStyleName("serverResponseLabelError");
						serverResponseLabel.setHTML(SERVER_ERROR);
						dialogBox.center();
						closeButton.setFocus(true);
					}

					public void onSuccess(String result) {
						dialogBox.setText("Remote Procedure Call");
						serverResponseLabel.removeStyleName("serverResponseLabelError");
						serverResponseLabel.setHTML(result);
						dialogBox.center();
						closeButton.setFocus(true);
					}
				});
			}
		}

		// Add a handler to send the name to the server
		MyHandler handler = new MyHandler();
		
		
		// Guestbook List
		dockPanel = new DockPanel();
		rootPanel.add(dockPanel, 0, 73);
		dockPanel.setSize("741px", "393px");

		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.setSpacing(5);
		dockPanel.add(horizontalPanel, DockPanel.NORTH);
		horizontalPanel.setWidth("742px");

		Button addButton = new Button("新增");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				AddPostDialog addDialog = new AddPostDialog();
				addDialog.center();
			}
		});

		horizontalPanel.add(nameField);
		nameField.setText("GWT User");
		nameField.setSize("524px", "16px");
		
		// Focus the cursor on the name field when the app loads
		nameField.setFocus(true);
		nameField.selectAll();
		nameField.addKeyUpHandler(handler);
		
        // We can add style names to widgets
		horizontalPanel.add(sendButton);
        sendButton.addStyleName("sendButton");
        sendButton.setSize("46px", "30px");
        sendButton.addClickHandler(handler);
		horizontalPanel.add(addButton);

		Button updateButton = new Button("修改");
		updateButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final DialogBox dialog = new DialogBox();
				final PostEditor editor = new PostEditor();
				editor.setPost(posts.get(selectRow));
				dialog.setWidget(editor);
				Button okButton = new Button("編輯");
				Button cancelButton = new Button("取消");
				okButton.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						Post post = editor.getPost();
						PostServiceAsync postService = (PostServiceAsync) GWT.create(PostService.class);
						postService.updatePost(post, new AsyncCallback<Boolean>() {
							@Override
							public void onSuccess(Boolean result) {
								if (result)
									Window.alert("修改成功");
								else
									Window.alert("修改失敗");
								dialog.hide();
							}

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("呼叫遠端程序失敗");
							}
						});
					}
				});
				cancelButton.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						dialog.hide();
					}
				});

				HorizontalPanel hPanel = new HorizontalPanel();
				hPanel.add(okButton);
				hPanel.add(cancelButton);
				editor.getDockPanel().add(hPanel, DockPanel.SOUTH);
				dialog.center();
			}
		});
		horizontalPanel.add(updateButton);

		Button deleteButton = new Button("刪除");
		deleteButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final DialogBox dialog = new DialogBox();
				Button confirmButton = new Button("確認刪除");
				Button cancelButton = new Button("取消");
				confirmButton.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						PostServiceAsync postService = (PostServiceAsync) GWT.create(PostService.class);
						postService.deletePost(posts.get(selectRow), new AsyncCallback<Boolean>() {
							@Override
							public void onSuccess(Boolean result) {
								if (result)
									Window.alert("刪除成功");
								else
									Window.alert("刪除失敗");
								dialog.hide();
							}

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("呼叫遠端程序失敗");
								dialog.hide();
							}
						});
					}
				});

				cancelButton.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						dialog.hide();
					}
				});

				HorizontalPanel hPanel = new HorizontalPanel();
				hPanel.add(confirmButton);
				hPanel.add(cancelButton);

				dialog.setText("是否刪除此留言?");
				dialog.setWidget(hPanel);
				dialog.setSize("200px", "150px");
				dialog.center();
			}
		});
		horizontalPanel.add(deleteButton);

		Button button = new Button("留言清單");
		button.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				readPosts();
				// setCenterWidget(null);
			}
		});
		// horizontalPanel.add(button);
		
		flexTable = new FlexTable();
		flexTable.addClickHandler(this);
		dockPanel.add(flexTable, DockPanel.CENTER);
		
		// UiBind ContactWidget 
		ContactWidget contactWidget = new ContactWidget();
		dockPanel.add(contactWidget, DockPanel.SOUTH);
		contactWidget.setContact(contact);
		contactWidget.setSize("327px", "111px");
		readPosts();
	}

	private void readPosts() {
		// 讀取所有留言清單
		PostServiceAsync postService = (PostServiceAsync) GWT.create(PostService.class);
		postService.getAllPosts(new AsyncCallback<List<Post>>() {
			@Override
			public void onSuccess(List<Post> result) {
				flexTable.clear();
				posts = result;
				for (int i = 0; i < result.size(); i++) {
					Post p = result.get(i);
					flexTable.setWidget(i, 0, new Label(p.getId() + ""));
					flexTable.setWidget(i, 1, new Label(p.getNickname()));
					flexTable.setWidget(i, 2, new Label(p.getTitle()));
					flexTable.setWidget(i, 3, new Label(p.getDate() + ""));
				}
				System.out.println("table set");
			}

			@Override
			public void onFailure(Throwable caught) {

			}
		});
	}
	
	public Widget getCenterWidget() {
		return centerWidget;
	}
	
	@Override
	public void onClick(ClickEvent event) {
		Cell cell = flexTable.getCellForEvent(event);

		// 取得儲存格所在的列數
		int row = cell.getRowIndex();

		// 處理選擇陰影樣式
		if (selectRow != -1) {
			flexTable.getRowFormatter().removeStyleName(selectRow, "selectedRow");
		}
		flexTable.getRowFormatter().addStyleName(row, "selectedRow");
		selectRow = row;

		// 利用列數由留言集合中取得留言物件
		Post p = posts.get(row);

		// 顯示留言明細
		Detail detail = new Detail();
		detail.setPost(p);

		DialogBox dialog = new DialogBox();
		dialog.add(detail);
		dialog.setAutoHideEnabled(true);
		dialog.center();
	}	
}
