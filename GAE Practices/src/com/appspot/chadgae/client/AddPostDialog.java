package com.appspot.chadgae.client;

import java.util.Date;

import com.appspot.chadgae.shared.Post;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;

public class AddPostDialog extends DialogBox {
	private TextBox nicknameTextBox;
	private TextBox titleTextBox;
	private TextArea contextTextArea;

	public AddPostDialog() {
		DockPanel dockPanel = new DockPanel();
		setWidget(dockPanel);
		dockPanel.setSize("375px", "258px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		dockPanel.add(horizontalPanel, DockPanel.NORTH);
		dockPanel.setCellHorizontalAlignment(horizontalPanel, HasHorizontalAlignment.ALIGN_CENTER);
		
		Label label_3 = new Label("新增留言");
		label_3.setStyleName("dialog-Title");
		horizontalPanel.add(label_3);
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		horizontalPanel_1.setSpacing(10);
		dockPanel.add(horizontalPanel_1, DockPanel.SOUTH);
		dockPanel.setCellHorizontalAlignment(horizontalPanel_1, HasHorizontalAlignment.ALIGN_CENTER);
		
		Button saveButton = new Button("新增");
		saveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Date now = new Date();
				String nickname = nicknameTextBox.getText();
				String title = titleTextBox.getText();
				String context = contextTextArea.getText();
				Post post = new Post(nickname, title, context, now);
				// 傳遞資料
				PostServiceAsync postService = (PostServiceAsync) GWT.create(PostService.class);
				postService.savePost(post, new AsyncCallback<Boolean>() {
					@Override
					public void onSuccess(Boolean result) {
						hide();
						Window.alert((result ? "留言已儲存" : "儲存失敗"));
					}

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("遠端呼叫失敗");
					}
				});
			}
		});
		horizontalPanel_1.add(saveButton);
		
		Button cancelButton = new Button("取消");
		cancelButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				hide();
			}
		});
		horizontalPanel_1.add(cancelButton);
		
		FlexTable flexTable = new FlexTable();
		dockPanel.add(flexTable, DockPanel.CENTER);
		flexTable.setSize("364px", "194px");
		
		Label label = new Label("匿稱:");
		flexTable.setWidget(0, 0, label);
		nicknameTextBox = new TextBox();
		flexTable.setWidget(0, 1, nicknameTextBox);
		
		Label label_1 = new Label("標題:");
		flexTable.setWidget(1, 0, label_1);
		titleTextBox = new TextBox();
		flexTable.setWidget(1, 1, titleTextBox);
		
		Label label_2 = new Label("留言內容:");
		flexTable.setWidget(2, 0, label_2);
		contextTextArea = new TextArea();
		flexTable.setWidget(2, 1, contextTextArea);
		contextTextArea.setSize("272px", "100px");
	}
}
