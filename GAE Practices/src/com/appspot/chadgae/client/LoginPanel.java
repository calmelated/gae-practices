package com.appspot.chadgae.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;

public class LoginPanel extends Composite {
	public LoginPanel() {

		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		horizontalPanel.setStyleName("login-panel");
		horizontalPanel.setSpacing(5);
		initWidget(horizontalPanel);
		horizontalPanel.setWidth("766px");

		Label label = new Label("帳號:");
		horizontalPanel.add(label);

		TextBox userIdTextBox = new TextBox();
		horizontalPanel.add(userIdTextBox);
		userIdTextBox.setWidth("125px");

		Label lblNewLabel = new Label("密碼:");
		horizontalPanel.add(lblNewLabel);

		PasswordTextBox passwordTextBox = new PasswordTextBox();
		horizontalPanel.add(passwordTextBox);
		passwordTextBox.setWidth("120px");

		Button loginButton = new Button("New button");
		loginButton.setText("登入");
		horizontalPanel.add(loginButton);

//		Hyperlink googeLink = new Hyperlink("Google Login", false, "newHistoryToken");
//		horizontalPanel.add(googeLink);
		
		Button googleLogin = new Button();
		googleLogin.setText("Google");
		googleLogin.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Window.Location.assign(GWT.getHostPageBaseURL() + "google");
			}
		});
		horizontalPanel.add(googleLogin);
		
		Button fbButton = new Button();
		fbButton.setText("Facebook");
		fbButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Window.Location.assign(GWT.getHostPageBaseURL() + "facebook");
			}
		});
		horizontalPanel.add(fbButton);		
		
		Button regButton = new Button();
		regButton.setText("Register");
		regButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Window.Location.assign(GWT.getHostPageBaseURL() + "registration.jsp");
			}
		});
		horizontalPanel.add(regButton);				
		
		Button amButton = new Button();
		amButton.setText("Query");
		amButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Window.Location.assign(GWT.getHostPageBaseURL() + "query-members.jsp");
			}
		});
		horizontalPanel.add(amButton);			
		
		Button logButton = new Button();
		logButton.setText("Login");
		logButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Window.Location.assign(GWT.getHostPageBaseURL() + "login.jsp");
			}
		});
		horizontalPanel.add(logButton);			
	}

}
