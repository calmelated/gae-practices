package com.appspot.chadgae.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MailServlet extends HttpServlet {
	public static final Logger log = Logger.getLogger(MailServlet.class.getName());

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = new PrintWriter(resp.getWriter());
		Properties prop = new Properties();
		Session session = Session.getDefaultInstance(prop);
		try {
			InternetAddress from = new InternetAddress("calm.elated@gmail.com");
			InternetAddress to = new InternetAddress("calmelated@gmail.com");
			Message message = new MimeMessage(session);
			message.setFrom(from);
			message.setRecipient(Message.RecipientType.TO, to);
			message.setSubject("mail for testing");
			message.setText("testtest");
			
			Transport.send(message);
			out.println("testtest");
			log.info("testtest");
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
			out.println("error trace");
			log.warning("error trace");
		}
	}
}
