package com.appspot.chadgae.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FacebookServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    String callbackURL = System.getProperty("fb_callback_url");
		String appId = System.getProperty("fb_app_id");
		String fbLoginURL = "https://graph.facebook.com/oauth/authorize" + "?client_id=" + appId + "&redirect_uri=" + callbackURL;
		resp.sendRedirect(fbLoginURL);
	}
}
