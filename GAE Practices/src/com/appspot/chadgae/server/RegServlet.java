package com.appspot.chadgae.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jdt.internal.compiler.ast.ThisReference;

import com.appspot.chadgae.shared.Member;
import com.appspot.chadgae.shared.PMF;

public class RegServlet extends HttpServlet {
	private final Logger log = Logger.getLogger(this.getClass().getName());

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String userid = req.getParameter("userid");
		String password = req.getParameter("password");
		String password2 = req.getParameter("password2");
		String nickname = req.getParameter("nickname");
		String email = req.getParameter("email");
		Member member = new Member(userid, password, nickname, email);
		log.info("Registor info: " + userid + "," + password + "," + nickname + "," + email);

		
		PrintWriter out = resp.getWriter();
		try {
			PersistenceManager pm = PMF.get().getPersistenceManager();
			pm.makePersistent(member);
			out.println("Success to register");
			log.info("Success to register");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
