package com.appspot.chadgae.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jdt.internal.compiler.ast.ThisReference;

public class MailServiceServlet extends HttpServlet {
	public final Logger log = Logger.getLogger(this.getClass().getName());

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Properties prop = new Properties();
		Session session = Session.getDefaultInstance(prop);
		try {
			Message message = new MimeMessage(session, req.getInputStream());
			String subject = message.getSubject();
			String content = getText(message);
			log.info("e:" + content);
			
			Address[] addrs = message.getFrom();
			log.info(subject);
			for (Address address : addrs)
				log.info(address.toString());
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public String getText(Part part) throws MessagingException, IOException {
		String content = null;
		if (part.isMimeType("text/*")) {
			log.info("CONTENT is text/plain");
			content = (String) part.getContent();
		} else if (part.isMimeType("multipart/alternative")) {
			log.info("CONTENT is multipart/alternative");
			Multipart mpart = (Multipart) part.getContent();
			for (int i = 0; i < mpart.getCount(); i++) {
				Part p = mpart.getBodyPart(i);
				content = getText(p);
				log.info("mpart " + i + ":" + content);
			}
		}
		return content;
	}
}
