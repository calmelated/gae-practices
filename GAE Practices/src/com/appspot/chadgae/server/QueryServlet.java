package com.appspot.chadgae.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.appspot.chadgae.shared.Contact;
import com.appspot.chadgae.shared.Member;
import com.appspot.chadgae.shared.PMF;

public class QueryServlet extends HttpServlet {
	private PrintWriter out;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		out = resp.getWriter();
		query0();
	}

	private void query0() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery("SELECT FROM " + Member.class.getName());
		List<Member> members = (List<Member>) query.execute();
		for (Member m : members) {
			out.println("<p>");
			out.println(m.getUserid() + "," + m.getNickname() + "," + m.getEmail());
			out.println("</p>");
		}
	}
	
	private void query1() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery("SELECT FROM " + Contact.class.getName());
		List<Contact> result = (List<Contact>) query.execute();
		for (Contact contact : result) {
			System.out.println(contact.getEmail());
		}
	}

	private void query2() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery("SELECT id FROM " + Contact.class.getName());
		List<Contact> ids = (List<Contact>) query.execute();
		for (Object id : ids) {
			System.out.println(id);
		}
	}

	private void query3() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery("SELECT FROM " + Contact.class.getName() + " WHERE name == 'Tom' ");
		List<Contact> result = (List<Contact>) query.execute();
		for (Contact contact : result) {
			System.out.println(contact.getEmail());
		}
	}

	private void query4() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery("SELECT FROM " + Contact.class.getName() + " WHERE name == p " + " PARAMETERS String p");
		List<Contact> result = (List<Contact>) query.execute("Tom");
		for (Contact contact : result) {
			System.out.println(contact.getEmail());
		}
	}

	private void query5() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery("SELECT FROM " + Contact.class.getName() + " ORDER BY name");
		List<Contact> result = (List<Contact>) query.execute();
		for (Contact contact : result) {
			System.out.println(contact.getName() + "," + contact.getEmail());
		}
	}

	private void q1() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery(Contact.class);
		List<Contact> result = (List<Contact>) query.execute();
		for (Contact contact : result) {
			System.out.println(contact.getEmail());
		}
	}

	private void q2() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery(Contact.class);
		query.setResult("id");
		List ids = (List) query.execute();
		for (Object id : ids) {
			System.out.println(id);
		}
	}

	private void q2_2() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery(Contact.class);
		query.setResult("id, name");
		List<Contact> result = (List<Contact>) query.execute();
		for (Contact contact : result) {
			System.out.println(contact.getId() + "," + contact.getName());
		}
	}

	private void q3() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery(Contact.class);
		query.setFilter("name==p");
		query.declareParameters("String p");
		List<Contact> result = (List<Contact>) query.execute("Tom");
		for (Contact contact : result) {
			System.out.println(contact.getId() + "," + contact.getName());
		}
	}

	private void q3_2() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery(Contact.class);
		query.setFilter("birthday==d");
		query.declareImports("import java.util.Date");
		query.declareParameters("Date d");
		// query.declareParameters("java.util.Date d");
		List<Contact> result = (List<Contact>) query.execute(new java.util.Date());
		for (Contact contact : result) {
			System.out.println(contact.getId() + "," + contact.getName());
		}
	}

	private void q4() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery(Contact.class);
		query.setOrdering("name ascending");
		List<Contact> result = (List<Contact>) query.execute();
		for (Contact contact : result) {
			System.out.println(contact.getEmail());
		}
	}	
}