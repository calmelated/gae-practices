package com.appspot.chadgae.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.appspot.chadgae.shared.PMF;
import com.appspot.chadgae.shared.Post;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.appspot.chadgae.client.PostService;

public class PostServiceImpl extends RemoteServiceServlet implements PostService {

	@Override
	public boolean savePost(Post post) {
		boolean saved = false;
		try {
			PersistenceManager pm = PMF.get().getPersistenceManager();
			pm.makePersistent(post);
			saved = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return saved;
	}

	@Override
	public boolean updatePost(Post post) {
		boolean updated = false;
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Post p = (Post) pm.getObjectById(Post.class, post.getId());
		try {
			p.setNickname(post.getNickname());
			p.setTitle(post.getTitle());
			p.setContent(post.getContent());
			p.setDate(new Date());
			updated = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			pm.close();
		}
		return updated;
	}

	@Override
	public List<Post> getAllPosts() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery("SELECT FROM " + Post.class.getName());
		List<Post> results = (List<Post>) query.execute();
		List<Post> posts = new ArrayList<Post>();
		for (Post p : results) {
//			pm.makePersistent(p);
//			p = pm.detachCopy(p);
			p.getNickname();
			posts.add(p);
		}
		pm.close();
		return posts;
	}

	@Override
	public boolean deletePost(Post post) {
		boolean deleted = false;
		PersistenceManager pm = PMF.get().getPersistenceManager();
		try {
			Post p = (Post) pm.getObjectById(Post.class, post.getId());
			pm.deletePersistent(p);
			deleted = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			pm.close();
		}
		return deleted;
	}
}
