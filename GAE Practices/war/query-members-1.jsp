<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.appspot.chadgae.shared.PMF"%>
<%@page import="com.appspot.chadgae.shared.Member"%>
<%@page import="java.util.List"%>
<%@page import="javax.jdo.Query"%>
<%@page import="javax.jdo.PersistenceManager"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
</head>
<body>
<%
    PersistenceManager pm = PMF.get().getPersistenceManager();
    Query query = pm.newQuery("SELECT FROM "+Member.class.getName());
    List<Member> members = (List<Member>) query.execute();
    for (Member m : members){
        out.println("<p>");
        out.println(m.getUserid()+","+m.getNickname()+","+m.getEmail());
        out.println("</p>");
    }
%>
</body>
</html>