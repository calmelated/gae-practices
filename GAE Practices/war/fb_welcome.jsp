<%@page import="java.util.List"%>
<%@page import="com.restfb.Connection"%>
<%@page import="com.restfb.types.User"%>
<%@page import="com.restfb.DefaultFacebookClient"%>
<%@page import="com.restfb.FacebookClient"%>
<%@page import="com.appspot.chadgae.shared.URLFetcher"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
</head>
<body>
	<%
		//取得授權碼
		String code			= request.getParameter("code");
		String client_id	= System.getProperty("fb_app_id");
		String redirect_uri = System.getProperty("fb_callback_url");
		String secret 		= System.getProperty("fb_secret");
		String authURL 		= "https://graph.facebook.com/oauth/access_token?" + "client_id=" + client_id + "&redirect_uri=" + redirect_uri + "&client_secret=" + secret + "&code=" + code;
		String resp 		= URLFetcher.get(authURL);
		String access_token = resp.substring(resp.indexOf("=") + 1, resp.lastIndexOf("&"));
	%>
	<p>
		授權碼:
		<%=code%></p>
	歡迎
	<p>
		存取符記(access token):
		<%=access_token%></p>
	<%
		FacebookClient fbClient = new DefaultFacebookClient(access_token);
		User user = fbClient.fetchObject("me", User.class);
		Connection<User> friends = fbClient.fetchConnection("me/friends", User.class);
		List<User> friendList = friends.getData();
		//for (int i=0; i<friendList.size(); i++)
		//	out.println(i+":"+friendList.get(i).getName()+"<br/>");
	%>
	帳號:
	<%=user.getName()%>
	<br /> E-mail:
	<%=user.getEmail()%>
	<br />
	<p>
		朋友個數:
		<%=friendList.size()%>
		<br />
		<%=friendList.get(173)%>
	</p>
</body>
</html>