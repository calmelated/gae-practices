<%@page import="com.google.appengine.api.users.User"%>
<%@page import="com.google.appengine.api.users.UserServiceFactory"%>
<%@page import="com.google.appengine.api.users.UserService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>帳號登入</title>
</head>
<body>
	<%
        String fbBackURL = System.getProperty("fb_callback_url");
        String fbAppId 	= System.getProperty("fb_app_id");
		String loginURL = null; // Google login URL 
		String fbLoginURL = "https://www.facebook.com/dialog/oauth?" + 
							"client_id=" + fbAppId + "&" + 
							"redirect_uri=" + fbBackURL;
	
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		if (user != null) {
			response.sendRedirect("/welcome.jsp");
		} else {
			loginURL = userService.createLoginURL("/login.jsp");
		}
	%>
	<table>
		<tr>
			<td>帳號:</td>
			<td><input type="text" name="userId" /></td>
		</tr>
		<tr>
			<td>密碼:</td>
			<td><input type="password" name="password" /></td>
		</tr>
		<tr>
			<td colspan="1"><input type="submit" value="登入" /></td>
		</tr>
		<tr>
			<td> <a href="<%=loginURL%>">Google</a> </td>
			<td> <a href="<%=fbLoginURL%>">Facebook<a> </td>
		</tr>
	</table>
</body>
</html>