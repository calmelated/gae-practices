<%@page import="com.appspot.chadgae.shared.Member" %>
<%@page import="com.appspot.chadgae.shared.PMF" %>
<%@page import="javax.jdo.PersistenceManager"%>
<%@page import="com.google.appengine.api.users.User"%>
<%@page import="com.google.appengine.api.users.UserServiceFactory"%>
<%@page import="com.google.appengine.api.users.UserService"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Welcome Page</title>
</head>
<body>
	<%
		String logoutURL = null;
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		if (user == null) {
			response.sendRedirect("/login.jsp");
		} else {
			logoutURL = userService.createLogoutURL("/login.jsp");
			Member member = new Member(user.getEmail(), null, user.getEmail());
			if (member.save()) {
				out.println("<p>[帳號新增完成]<p>");
			} else {
				out.println("<p>[已有帳號]<p>");
			}
		}
	%>
	Welcome,
	<%=user.getEmail()%>
	<a href="<%=logoutURL%>">登出Google帳號</a>
</body>
</html>